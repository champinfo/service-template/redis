# Docker-Compose

## Build from outside projects

1. Please check the shared network name of the outside project.

2. Please check `./replication/docker-compose.yml` file.

## Build images for k8s

1. Please check `.gitlab-ci.yml`

2. Please check `docker-compose-prod.yml`.

## Build images for cluster

1. Please check `./cluster/docker-compose.yml`

## Ref

1. [Redis Master-Slave Replication Instance](https://blog.yowko.com/docker-redis-master-slave-replication/)

2. [Redis Cluster](https://blog.yowko.com/docker-compose-redis-cluster/)
