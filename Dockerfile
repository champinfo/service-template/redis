FROM redis:latest
LABEL maintainer="Isaac Cheng <isaac@championtek.com.tw>"

RUN apt-get install bash \
    sed

RUN mkdir /redis-master && \
    mkdir /redis-slave

# the destination dir should be the same as in the entrypoint.sh
COPY redis-master.conf /redis-master/redis.conf
COPY redis-slave.conf /redis-slave/redis.conf
COPY entrypoint.sh /entrypoint.sh

RUN chmod 777 /entrypoint.sh

CMD [ "/entrypoint.sh" ]

ENTRYPOINT [ "bash", "-C" ]